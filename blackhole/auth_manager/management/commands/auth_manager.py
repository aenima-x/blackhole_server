# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils import timezone
from django.db import connection
from pyrad.server import Server, RemoteHost
from pyrad.dictionary import Dictionary
import ldap
from twisted.internet import protocol
from twisted.internet import reactor
from twisted.internet.threads import deferToThread
from pyrad import dictionary
from pyrad import host
from pyrad import packet

from auth_manager.models import LocalUser, RadiusClient

logger = logging.getLogger(__name__)


def defer_to_thread(func):
    def wrapper(*args, **kw):
        return deferToThread(func, *args, **kw)
    return wrapper


class PacketError(Exception):
    """Exception class for bogus packets
    PacketError exceptions are only used inside the Server class to
    abort processing of a packet.
    """


class RADIUS(host.Host, protocol.DatagramProtocol):
    def __init__(self, hosts=None, dict=dictionary.Dictionary()):
        if hosts is None:
            hosts = {}
        host.Host.__init__(self, dict=dict)
        self.hosts = hosts

    def processPacket(self, pkt):
        pass

    def createPacket(self, **kwargs):
        raise NotImplementedError("Attempted to use a pure base class")

    def datagramReceived(self, datagram, host_data):
        host, port = host_data
        logger.info(f"New Connection from: {host}:{port}")
        try:
            pkt = self.CreatePacket(packet=datagram)
        except packet.PacketError as err:
            logger.info(f"Dropping invalid packet: {err}")
            return
        if host not in self.hosts:
            logger.info(f"Dropping packet from unknown host: {host}")
            return
        pkt.source = (host, port)
        try:
            self.processPacket(pkt)
        except PacketError as err:
            logger.info(f"Dropping packet from {host}: {err}")

    @staticmethod
    def validate_local_user(username, password):
        logger.debug("Validating Local User")
        try:
            user = LocalUser.objects.get(username=username, enabled=True)
        except LocalUser.DoesNotExist as e:
            logger.info(f"Unknown Local User: {username}")
            return False
        except Exception as e:
            logger.error(f"Error searching local user {e}")
            return False
        else:
            logger.debug(f"User {username} Exists")
            auth_ok = (user.password == password)
            logger.debug(f"Password for user {username} valid: {auth_ok}")
            return auth_ok


    @staticmethod
    def validate_ad_user(domain, username, password):
        logger.debug(f"Validating AD User: {domain.name}")
        is_valid = False
        try:
            l = ldap.initialize(domain.get_domain_controller())
            l.simple_bind_s(f"{username}@{domain.name}", password)
            logger.info(f"Valid {domain} user ({l.whoami_s()})")
            l.unbind_s()
            is_valid = True
        except ldap.INVALID_CREDENTIALS:
            logger.debug(f"Invalid AD Credentials for user={username} on domain={domain}")
            is_valid = False
        except Exception as e:
            logger.error(f"{domain}/{username} - {e}")
            is_valid = False
        return is_valid

    @defer_to_thread
    def validate_user(self, radius_client, username, password):
        is_valid = False
        if radius_client.valida_local_users:
            is_valid = self.validate_local_user(username, password)
        for domain in radius_client.domains.all():
            is_valid = self.validate_ad_user(domain, username, password)
            if is_valid:
                break
        return is_valid


class RADIUSAccess(RADIUS):

    def CreateReplyPacket(self, pkt, **attributes):
        """Create a reply packet.
        Create a new packet which can be returned as a reply to a received
        packet.

        :param pkt:   original packet
        :type pkt:    Packet instance
        """
        reply = pkt.CreateReply(**attributes)
        reply.source = pkt.source
        return reply

    def CreatePacket(self, **kwargs):
        return self.CreateAuthPacket(**kwargs)

    def processPacket(self, pkt):
        radius_client_data = self.hosts[pkt.source[0]]
        pkt.secret = radius_client_data.secret.encode()
        if pkt.code != packet.AccessRequest:
            raise PacketError("non-AccessRequest packet on authentication socket")
        else:
            try:
                username = pkt["User-Name"][0]
                password_index = 2
                crypt_password = pkt.get(password_index)[0]
                plain_password = pkt.PwDecrypt(crypt_password)
                if username == "" or plain_password == "":
                    logger.error("Invalid user or password")
                    return
            except Exception as e:
                logger.error(f"Dropping packet: Invalid Authenticator data from ({pkt.source[0]}) {e} ")
                logger.error(f"Authenticator: {pkt}")
                return
            else:
                try:
                    connection.close()
                    try:
                        radius_client = RadiusClient.objects.get(id=radius_client_data.name)
                    except RadiusClient.DoesNotExist:
                        logger.error(f"Invalid Client: {radius_client_data.name}")
                    else:
                        logger.debug(f"Validating Client: {radius_client.name} ({radius_client_data.address}) - User:{username}")
                        reply = self.CreateReplyPacket(pkt)
                        self.request = {"pkt": pkt, "username": username, "reply": reply}

                        def process_response(is_valid):
                            if is_valid:
                                logger.info(f"User ({username}) Valid")
                                reply.code = packet.AccessAccept
                            else:
                                logger.info(f"User ({username}) Rejected")
                                reply.code = packet.AccessReject
                            self.transport.write(reply.ReplyPacket(), pkt.source)
                        d = self.validate_user(radius_client, username, plain_password)
                        d.addCallback(process_response)
                except Exception as e:
                    logger.error(f"Dropping packet: {e}")


    # @defer_to_thread
    # def validate_user(self, username, plain_password):
    #     if username == "" or plain_password == "":
    #         return False
    #     if validate_local_user(username, plain_password):
    #         logger.info(f"Local valid user ({username})")
    #         return True
    #     elif validate_ldap(username, plain_password, settings.TMOVILES_DOMAIN, settings.TMOVILES_DC):
    #         logger.info(f"Tmoviles valid user ({username})")
    #         return True
    #     elif validate_ldap(username, plain_password, settings.PLATAFORMAS_DOMAIN, settings.PLATAFROMAS_DC):
    #         logger.info("Plataformas valid user ({0})".format(username))
    #         return True
    #     elif validate_ldap(username, plain_password, settings.TASA_DOMAIN, settings.TASA_DC):
    #         logger.info("Tasa valid user ({0})".format(username))
    #         return True
    #     else:
    #         return None


class Command(BaseCommand):
    args = "Auth Manager Radius Server"

    def handle(self, *args, **options):
        logger.info("Start Auth Manager Server")
        dictionary_file = os.path.join(settings.BASE_DIR, "auth_manager", "dictionary")
        hosts = {x.address: RemoteHost(x.address, x.secret, x.id) for x in RadiusClient.objects.filter(enabled=True)}
        reactor.listenUDP(1812, RADIUSAccess(dict=Dictionary(dictionary_file), hosts=hosts))
        reactor.run()
