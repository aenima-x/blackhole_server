from django.urls import path
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

import web.routing

application = ProtocolTypeRouter({
    # Empty for now (http->django views is added by default)
    'websocket':
            AuthMiddlewareStack(
                URLRouter(
                        web.routing.websocket_urlpatterns
                )
            )
})