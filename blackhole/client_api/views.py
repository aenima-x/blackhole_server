import json
import logging
import time

import redis
from django.conf import settings
from rest_framework.decorators import action
from rest_framework.response import Response
from web.models import AppUser, SecureShellConnection, DatabaseConnection, PrivateKey, SessionRecord, Target
from rest_framework import status, viewsets, mixins

from .serializer import AppUserSerializer, FullSecureShellConnectionSerializer, FullDatabaseConnectionSerializer, \
    PrivateKeySerializer, AppUserEnabledStatusSerializer, SessionRecordSerializer
from .permissions import CustomGetPermission
from django.utils import timezone


logger = logging.getLogger(__name__)


class PrivateKeyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PrivateKey.objects.all()
    serializer_class = PrivateKeySerializer
    depth = 1
    permission_classes = (CustomGetPermission,)


class SecureShellConnectionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SecureShellConnection.objects.all()
    serializer_class = FullSecureShellConnectionSerializer
    depth = 1
    permission_classes = (CustomGetPermission,)

    @action(methods=['get'], detail=True, permission_classes=[CustomGetPermission])
    def private_key(self, request, pk=None):
        try:
            secure_shell_connection = SecureShellConnection.objects.get(pk=pk)
        except SecureShellConnection.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            if secure_shell_connection.authentication_method == SecureShellConnection.AUTHENTICATION_METHOD_PRIVATE_KEY:
                try:
                    private_key = PrivateKey.objects.get(environment=secure_shell_connection.target.environment, user=secure_shell_connection.user)
                    serializer = PrivateKeySerializer(private_key)
                    return Response(serializer.data)
                except PrivateKey.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)


class DatabaseConnectionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = DatabaseConnection.objects.all()
    serializer_class = FullDatabaseConnectionSerializer
    depth = 1
    permission_classes = [CustomGetPermission]


# class RDPConnectionViewSet(viewsets.ReadOnlyModelViewSet):
#     queryset = RDPConnection.objects.all()
#     serializer_class = FullRDPConnectionSerializer
#     depth = 1
#     permission_classes = (CustomGetPermission,)


class AppUserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AppUser.objects.all()
    serializer_class = AppUserSerializer
    depth = 1
    lookup_field = "username"
    permission_classes = (CustomGetPermission,)

    @action(methods=['get'], detail=True, permission_classes=[CustomGetPermission])
    def allowed_to_connect(self, request, username=None):
        try:
            user = AppUser.objects.get(username=username)
            if "target_id" in request.GET:
                target_id = request.GET["target_id"]
            else:
                target_id = None
            if target_id:
                try:
                    target_connection = SecureShellConnection.objects.get(id=target_id)
                    is_allowed = user.is_allowed_to_connect(target_connection=target_connection)
                    return Response({'allowed': is_allowed})
                except Target.DoesNotExist:
                    logger.error(f"Unknown Target ID: {target_id}")
                    return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                is_allowed = user.is_allowed_to_connect()
                return Response({'allowed': is_allowed})
        except AppUser.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class SessionRecordViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    queryset = SessionRecord.objects.all()
    serializer_class = SessionRecordSerializer
    permission_classes = (CustomGetPermission,)

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        session_record_id = response.data['id']
        login_date = time.mktime(timezone.now().timetuple())
        redis_session_data = {"username": request.data['username'],
                              "session_id": request.data['session_id'],
                              "client_pid": request.data['client_pid'],
                              "source_ip": request.data['source_ip'],
                              "connection_type": request.data['connection_type'],
                              "target": request.data['target'],
                              "target_user": request.data['target_user'],
                              "log_file": request.data['log_file'],
                              "id": session_record_id,
                              "login_date": login_date
                              }
        redis_server = redis.Redis(settings.REDIS_SERVER)
        redis_server.set(settings.REDIS_CLIENTS_KEY % response.data['id'], json.dumps(redis_session_data))
        return response

    @action(methods=['get'], detail=True, permission_classes=[CustomGetPermission])
    def ended(self, request, pk):
        try:
            session_record = SessionRecord.objects.get(pk=pk)
            session_record.logout_date = timezone.now()
            session_record.save()
            redis_server = redis.Redis(settings.REDIS_SERVER)
            redis_server.delete(settings.REDIS_CLIENTS_KEY % pk)
            return Response({"updated": "OK"})
        except SessionRecord.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
