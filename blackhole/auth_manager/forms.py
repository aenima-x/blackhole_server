# -*- coding: utf-8 -*-
from django import forms

class RecoverPasswordForm(forms.Form):
    username = forms.CharField(label=u"Usuario / Username", max_length=30,
                               widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Usuario / Username', 'id':'recover_username'}))
    mail = forms.EmailField(label=u"Correo / Email",
                            widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Correo / Email', 'id':'recover_mail'}))


class ResetPasswordForm(forms.Form):
    username = forms.CharField(label=u"Usuario / Username", max_length=30,
                               widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Usuario / Username', 'id':'reset_username'}))
    old_password = forms.CharField(label=u"Password actual / Current Password", max_length=100,
                                   widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Password actual / Current Password', 'id':'reset_password'}))
    new_password = forms.CharField(label=u"Password nuevoi / New Password", max_length=100,
                                   widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Password nuevo / New Password', 'id':'new_reset_password'}))
    new_password_confirmation = forms.CharField(label=u"Password nuevo (confirmacion) / New Passwored (Confirmation)", max_length=100,
                                                widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Password nuevo (confirmacion) / New Passwored (Confirmation)', 'id':'new_reset_password_confirmation'}))
