from rest_framework import routers
from .views import SecureShellViewSet

router = routers.SimpleRouter()
router.register('secure_shell', SecureShellViewSet)


urlpatterns = [] + router.urls