from rest_framework import routers
from .views import AppUserViewSet, SecureShellConnectionViewSet, DatabaseConnectionViewSet,\
    PrivateKeyViewSet, SessionRecordViewSet


router = routers.SimpleRouter()
router.register('user', AppUserViewSet)
router.register('secure_shell', SecureShellConnectionViewSet)
router.register('database', DatabaseConnectionViewSet)
router.register('private_key', PrivateKeyViewSet)
router.register('session_record', SessionRecordViewSet)


urlpatterns = [] + router.urls