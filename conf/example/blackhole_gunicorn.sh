#!/bin/bash

NAME="blackhole"                                  # Name of the application
DJANGODIR=/vagrant/blackhole_server/blackhole           # Django project directory
USER=root                                   # the user to run as
GROUP=vagrant                                 # the group to run as
NUM_WORKERS=5                                    # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=blackhole.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=blackhole.wsgi                     # WSGI module name
LOG=/var/log/blackhole/blackhole_gunicorn.log
BIND_IP=127.0.0.1
BIND_PORT=8001
TIMEOUT=30
echo "Starting $NAME"

# Activate the virtual environment
#cd $DJANGODIR
source /home/vagrant/.virtualenvs/blackhole/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH


# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)

cd $DJANGODIR
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-file=$LOG \
  --bind=$BIND_IP:$BIND_PORT \
  --timeout=$TIMEOUT \
  #-D
