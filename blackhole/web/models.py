import logging
import pwd
import os

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils import timezone
from django.apps import apps

import paramiko
from encrypted_model_fields.fields import EncryptedCharField, EncryptedTextField


# Create your models here.


logger = logging.getLogger(__name__)


def no_white_space_validator(value):
    if " " in value:
        raise ValidationError('White spaces not allowed')


def get_time():
    return timezone.now().time().replace(second=0)


class Environment(models.Model):
    name = models.CharField(max_length=35, unique=True, verbose_name=_("Name"))

    def __str__(self):
        return self.name

    class Meta(object):
        verbose_name = _("Environment")
        verbose_name_plural = _("Environments")
        ordering = ['name']


class Target(models.Model):
    name = models.CharField(max_length=30, verbose_name=_("Name"), validators=[no_white_space_validator])
    ip = models.GenericIPAddressField(protocol='IPv4', verbose_name=_("IP Address"))
    description = models.CharField(max_length=150, blank=True, null=True, verbose_name=_("Description"))
    environment = models.ForeignKey(Environment, verbose_name=_("Environment"), on_delete=models.CASCADE)

    # def clean(self):
    #     license = apps.get_app_config('web').license
    #     if license.is_expired:
    #         raise ValidationError(_("Expired License"))
    #     else:
    #         if not (SecureShell.objects.all().count() + Database.objects.all().count()) < license.license_data['targets']:
    #             raise ValidationError(_("Max Number of targets reached"))

    def __str__(self):
        return f"{self.name}"

    class Meta(object):
        ordering = ['name']

class SecureShell(Target):
    DEVICE_TYPE_LINUX = 0
    DEVICE_TYPE_SOLARIS = 1
    DEVICE_TYPE_AIX = 2
    DEVICE_TYPE_NETWORK = 3
    DEVICE_TYPE_OSX = 4
    DEVICE_TYPE_OTHER = 5
    DEVICE_TYPE_CHOICES = ((DEVICE_TYPE_LINUX, 'Linux'),
                           (DEVICE_TYPE_SOLARIS, 'Solaris'),
                           (DEVICE_TYPE_AIX, 'AIX'),
                           (DEVICE_TYPE_NETWORK, 'Network Device'),
                           (DEVICE_TYPE_OSX, 'OSX'),
                           (DEVICE_TYPE_OTHER, 'Other'))
    port = models.PositiveIntegerField(default=22, verbose_name=_("Port"))
    device_type = models.SmallIntegerField(choices=DEVICE_TYPE_CHOICES, default=DEVICE_TYPE_LINUX, verbose_name=_("Device Type"))

    def __str__(self):
        return "{0.name} ({0.ip}) - [{0.environment}]".format(self)

    class Meta(object):
        verbose_name = _("Secure Shell Target")
        verbose_name_plural = _("Secure Shell Targets")
        ordering = ['description', 'name']


class Database(Target):
    ENGINE_MYSQL = 0
    ENGINE_ORACLE = 1
    ENGINE_POSTGRES = 2
    ENGINE_CHOICES = ((ENGINE_ORACLE, 'Oracle'),
                      (ENGINE_MYSQL, 'MySQL'),
                      (ENGINE_POSTGRES, 'Postgres'))
    db_name = models.CharField(max_length=50)
    port = models.PositiveIntegerField(default=3306, verbose_name=_("Port"))
    engine = models.SmallIntegerField(choices=ENGINE_CHOICES, default=ENGINE_MYSQL, verbose_name=_("Engine"))

    def __str__(self):
        return "{0.name} ({0.ip}) - [{0.environment}]".format(self)

    class Meta(object):
        verbose_name = _("Database Target")
        verbose_name_plural = _("Database Targets")
        ordering = ['description', 'name']


# class RemoteDesktop(Target):
#     port = models.PositiveIntegerField(default=3389, verbose_name=_("Port"))
#     secure_rdp = models.BooleanField(default=False, verbose_name=_("Secure RDP"))
#
#     def __str__(self):
#         return "{0.name} ({0.ip}) - [{0.environment}]".format(self)
#
#     class Meta(object):
#         verbose_name = _("RDP Target")
#         verbose_name_plural = _("RDP Targets")
#         ordering = ['name']


class PrivateKey(models.Model):
    RSA_KEY_TYPE = 0
    DSA_KEY_TYPE = 1
    TYPE_CHOICES = (
                    (RSA_KEY_TYPE, 'RSA'),
                    (DSA_KEY_TYPE, 'DSA'),
                    )
    user = models.CharField(max_length=30, validators=[no_white_space_validator], verbose_name=_("User"))
    environment = models.ForeignKey(Environment, verbose_name=_("Environment"), on_delete=models.CASCADE)
    key_type = models.IntegerField(choices=TYPE_CHOICES, default=RSA_KEY_TYPE, verbose_name=_("Key Type"))
    private_key = EncryptedTextField(verbose_name=_("Private Key"))
    public_key = EncryptedTextField(verbose_name=_("Public Key"))

    def __str__(self):
        return f"{_('User')}: {self.user} - {_('Environment')}: {self.environment.name} ({self.get_key_type_display()})"

    def readlines(self):
        pk_to_string = str(self.private_key).replace('\r', '')
        return pk_to_string.split('\n')

    class Meta(object):
        unique_together = ('user', 'environment')
        verbose_name = _("Private Key")
        verbose_name_plural = _("Private Keys")
        ordering = ['user', 'environment']


# class TargetConnection(models.Model):
#     target = models.ForeignKey(Target, verbose_name=_("Target"), on_delete=models.CASCADE)
#     user = models.CharField(max_length=30, validators=[no_white_space_validator], verbose_name=_("User"))
#
#     def get_connection(self):
#         target_class = type(self.target)
#         if target_class == SecureShell:
#             return self.secureshellconnection
#         elif target_class == Database:
#             return self.databaseconnection
#         elif target_class == RemoteDesktop:
#             return self.rdpconnection
#
#     class Meta(object):
#         unique_together = ('target', 'user')
#         ordering = ['target', 'user']



class SecureShellConnection(models.Model):
    AUTHENTICATION_METHOD_PRIVATE_KEY = 0
    AUTHENTICATION_METHOD_PASSWORD = 1
    AUTHENTICATION_METHODS_CHOICES = ((AUTHENTICATION_METHOD_PRIVATE_KEY, _('Private Key')),
                                      (AUTHENTICATION_METHOD_PASSWORD, _('Password')))
    authentication_method = models.SmallIntegerField(default=AUTHENTICATION_METHOD_PRIVATE_KEY,
                                                     choices=AUTHENTICATION_METHODS_CHOICES,
                                                     verbose_name=_("Authentication Method"))
    target = models.ForeignKey(SecureShell, verbose_name=_("Secure Shell"), on_delete=models.CASCADE)
    user = models.CharField(max_length=30, validators=[no_white_space_validator], verbose_name=_("User"))
    password = EncryptedCharField(max_length=100, verbose_name=_('Password'), blank=True, null=True)

    def __str__(self):
        return f"{self.target} as {self.user} using {self.get_authentication_method_display()}"

    def get_private_key(self, user):
        try:
            pk = PrivateKey.objects.get(user=user, environment=self.target.environment)
            try:

                if pk.key_type == PrivateKey.DSA_KEY_TYPE:
                    key = paramiko.DSSKey.from_private_key(pk)
                elif pk.key_type == PrivateKey.RSA_KEY_TYPE:
                    key = paramiko.RSAKey.from_private_key(pk)
            except Exception as e:
                logger.error("Invalid Private Key for user {%s} to host {%s} <{%}>", user, self.target, e.message)
                raise Exception("Invalid Private Key")
            else:
                return key
        except PrivateKey.DoesNotExist as e:
            logger.error("Missing Private Key for user {0} to host {1}".format(user, self.target))
            raise Exception("Missing Private Key")

    class Meta(object):
        unique_together = ('target', 'user')
        verbose_name = _("SecureShell Connection")
        verbose_name_plural = _("SecureShell Connections")
        ordering = ['target', 'user']


class DatabaseConnection(models.Model):
    target = models.ForeignKey(Database, verbose_name=_("Database"), on_delete=models.CASCADE)
    user = models.CharField(max_length=30, validators=[no_white_space_validator], verbose_name=_("User"))
    password = EncryptedCharField(max_length=100, verbose_name=_('Password'))

    def __str__(self):
        return f"{self.target} as {self.user}"

    def get_client_command(self):
        if self.target.engine == Database.ENGINE_MYSQL:
            return f"{settings.MYSQL_CLIENT} -u {self.user} -p{self.password} -D {self.target.db_name} -h {self.target.ip} -P {self.target.port}"
        elif self.target.engine == Database.ENGINE_ORACLE:
            return f"{settings.ORACLE_CLIENT} {self.user}/{self.password}@//{self.target.ip}:{self.target.port}/{self.target.db_name}"
        elif self.target.engine == Database.ENGINE_POSTGRES:
            return f"{settings.POSTGRES_CLIENT} 'postgresql://{self.user}:{self.password}@{self.target.ip}:{self.target.port}/{self.target.db_name}'"
        else:
            return ""

    class Meta(object):
        verbose_name = _("Database Connection")
        verbose_name_plural = _("Databases Connections")
        ordering = ['target', 'user']


# class RDPConnection(models.Model):
#     target = models.ForeignKey(SecureShell, verbose_name=_("Secure Shell"), on_delete=models.CASCADE)
#     user = models.CharField(max_length=30, validators=[no_white_space_validator], verbose_name=_("User"))
#     domain = models.CharField(max_length=50, verbose_name=_("Domain"))
#     password = EncryptedCharField(max_length=100, verbose_name=_('Password'))
#
#     def __str__(self):
#         return f"{self.target} as {self.domain}\{self.user}"
#
#     class Meta(object):
#         verbose_name = _("RDP Connection")
#         verbose_name_plural = _("RDP Connections")
#         ordering = ['target', 'user']


class Profile(models.Model):
    name = models.CharField(max_length=15, unique=True, verbose_name=_("Name"))
    secure_shell_connections = models.ManyToManyField(SecureShellConnection, blank=True, verbose_name=_("Secure Shell"))
    database_connections = models.ManyToManyField(DatabaseConnection, blank=True, verbose_name=_("Database"))
    #rdp_connections = models.ManyToManyField(RDPConnection, blank=True, verbose_name=_("RDP"))

    def has_target_connection(self, target_connection):
        target_connection_class = type(target_connection.target)
        logger.debug(f"Target {target_connection}: {target_connection_class.__name__}")
        if target_connection_class == SecureShell:
            target_connection_instance = SecureShellConnection.objects.get(id=target_connection.id)
            return target_connection_instance in self.secure_shell_connections.all()
        elif target_connection_class == Database:
            target_connection_instance = DatabaseConnection.objects.get(id=target_connection.id)
            return target_connection_instance in self.database_connections.all()
        # elif target_connection_class == RemoteDesktop:
        #     target_connection_instance = RDPConnection.objects.get(id=target_connection.id)
        #     return target_connection_instance in self.rdp_connections.all()

    def __str__(self):
        return self.name

    class Meta(object):
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")
        ordering = ['name']


class AppUser(models.Model):
    username = models.CharField(max_length=20, verbose_name=_("User"), db_index=True)
    first_name = models.CharField(max_length=50, verbose_name=_("Name"))
    last_name = models.CharField(max_length=50, verbose_name=_("Last Name"))
    description = models.CharField(max_length=20, blank=True, null=True, verbose_name=_("Description"))
    email = models.EmailField(verbose_name=_('Email'), blank=True, null=True)
    profile = models.ForeignKey(Profile, verbose_name=_("Profile"), on_delete=models.CASCADE)
    enabled = models.BooleanField(default=True, verbose_name=_("Enabled"))
    allowed_monday = models.BooleanField(default=True, verbose_name=_("Monday"))
    allowed_tuesday = models.BooleanField(default=True, verbose_name=_("Tuesday"))
    allowed_wednesday = models.BooleanField(default=True, verbose_name=_("Wednesday"))
    allowed_thursday = models.BooleanField(default=True, verbose_name=_("Thursday"))
    allowed_friday = models.BooleanField(default=True, verbose_name=_("Friday"))
    allowed_saturday = models.BooleanField(default=True, verbose_name=_("Saturday"))
    allowed_sunday = models.BooleanField(default=True, verbose_name=_("Sunday"))
    time_range_enabled = models.BooleanField(default=False, verbose_name=_("Enabled in Time Range"))
    time_range_enabled_since = models.TimeField(default=get_time, verbose_name=_("Since"))
    time_range_enabled_to = models.TimeField(default=get_time, verbose_name=_("Until"))
    manual_ssh = models.BooleanField(default=False, verbose_name=_("Manual ssh"))
    not_allowed_environments = models.ManyToManyField(Environment, blank=True, verbose_name=_("Not Allowed Environments"))
    notes = models.TextField(max_length=300, verbose_name=_("Notes"), blank=True, null=True)
    last_login = models.DateTimeField(verbose_name=_("Ultimo Login"), null=True, blank=True, editable=False)

    def get_full_name(self):
        return f"{self.last_name}, {self.first_name} ({self.username})"

    def __str__(self):
        return f"{self.last_name}, {self.first_name} ({self.username})"

    def clean(self):
        if self.username in settings.NOT_VALID_USERS:
            raise ValidationError({'username': "Invalid Username"})

    def is_allowed_to_connect(self, target_connection=None):

        def in_between(now, start, end):
            if start <= end:
                return start <= now < end
            else:  # over midnight e.g., 23:30-04:15
                return start <= now or now < end

        day_validators = [self.allowed_monday, self.allowed_tuesday, self.allowed_wednesday,
                            self.allowed_thursday, self.allowed_friday, self.allowed_saturday, self.allowed_sunday]
        logger.debug(f"Check if User={self.username} is allowed to Connect")
        if self.enabled:
            day_of_the_week = timezone.now().weekday()
            if day_validators[day_of_the_week]:
                if self.time_range_enabled:
                    if not in_between(timezone.now().time(), self.time_range_enabled_since, self.time_range_enabled_to):
                        logger.debug(f"User={self.username} is not allowed to connect at this time")
                        return False
                if target_connection:
                    if target_connection.target.environment in self.not_allowed_environments.all():
                        logger.debug(f"User={self.username} not allowed to Environment {target_connection.environment}")
                        return False
                    else:
                        target_connection_in_profile = self.profile.has_target_connection(target_connection)
                        logger.debug(f"User={self.username} has target in profile: {target_connection_in_profile}")
                        return target_connection_in_profile
                else:
                    return True
            else:
                logger.debug(f"User={self.username} is not allowed to connect Today")
                return False
        else:
            logger.debug(f"User={self.username} is disabled")
            return False

    class Meta(object):
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        ordering = ['username']


class SessionRecord(models.Model):
    username = models.CharField(max_length=20, verbose_name=_("Username"))
    session_id = models.CharField(verbose_name=_("Session UUID"), max_length=40)
    client_pid = models.IntegerField(verbose_name=_("Client PID"), default=0)
    source_ip = models.GenericIPAddressField(protocol='IPv4', verbose_name=_("Source IP"))
    connection_type = models.CharField(max_length=30, verbose_name=_("Connection Type"))
    target = models.CharField(max_length=50, verbose_name=_("Target"))
    target_user = models.CharField(max_length=30, verbose_name=_("Identified As"))
    login_date = models.DateTimeField(verbose_name=_("Login date"), auto_now_add=True)
    logout_date = models.DateTimeField(verbose_name=_("Logout date"), null=True, blank=True)
    log_file = models.CharField(max_length=250, verbose_name=_("Log File"))

    def __str__(self):
        return f"{self.username}[{self.target_user}] a {self.target} ({self.login_date})"

    class Meta(object):
        verbose_name = _("Session Record")
        verbose_name_plural = _("Session Records")
        ordering = ['login_date', 'username']


#Signals
@receiver(post_save, sender=AppUser)
def create_local_app_user(sender, instance, created, **kwargs):
    if settings.MANAGE_OS_USERS:
        if created:
            try:
                pwd.getpwnam(instance.username)
                logger.error("Local User [%s] already created", instance.username)
            except KeyError:
                logger.info("Creating local User [%s]", instance.username)
                cmd = f"{settings.CREATE_USER_SCRIPT} {instance.username.lower()} '{instance.last_name.title()}, {instance.first_name.title()}' {settings.BASE_DIR}"
                os.system(cmd.encode('utf-8'))


@receiver(post_delete, sender=AppUser)
def delete_local_app_user(sender, instance, using, **kwargs):
    if settings.MANAGE_OS_USERS:
        try:
            pwd.getpwnam(instance.username)
            if instance.username not in settings.NOT_VALID_USERS:
                logger.info("Deleting local user [%s]", instance.username)
                cmd = f"{settings.DELETE_USER_SCRIPT} {instance.username.lower()}"
                os.system(cmd.encode('utf-8'))
            else:
                logger.error("Trying to delete system user [%s]", instance.username)
        except KeyError:
            logger.info("Local user [%s] don't exists", instance.username)
