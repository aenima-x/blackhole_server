#!/usr/bin/env python
import redis
import os
import sys


def view_session(session_uuid, pid):
    print("Enable Streaming...")
    os.kill(pid, 10)
    try:
        r = redis.StrictRedis(host='localhost', port=6379, decode_responses=True)
        p = r.pubsub()
        p.subscribe(f"sessions.{session_uuid}")
        print("[Play...]")
        for item in p.listen():
            if item['type'] == 'message':
                sys.stdout.write(item['data'])
    except KeyboardInterrupt:
        print("Stop Streaming...")
        os.kill(pid, 12)
    except Exception as e:
        print(e)



if __name__ == "__main__":
    if len(sys.argv) == 3:
        uuid = sys.argv[1]
        pid = int(sys.argv[2])
        view_session(uuid, pid)
    else:
        print(f"Usage: {sys.argv[0]} UUID PID")