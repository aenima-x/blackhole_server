from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Permission
from django.conf import settings
from rest_framework.authtoken.models import Token
import string

class Command(BaseCommand):
    help = "Blackhole Initial Setup."

    def handle(self, *args, **options):
        print("Create Client User")
        client_permissions = ['view_appuser', 'view_privatekey', 'view_secureshellconnection',
                              'view_databaseconnection', 'view_sessionrecord', 'add_sessionrecord']
        user, created = User.objects.get_or_create(username=settings.CLIENT_USER, email="some@mail.com")
        if created:
            password = User.objects.make_random_password(length=30, allowed_chars=string.digits + string.ascii_letters)
            user.set_password(password)
            print("Add Default Permissions to client")
            for permission_codename in client_permissions:
                permission = Permission.objects.get(codename=permission_codename)
                user.user_permissions.add(permission)
            user.save()
            token = Token.objects.create(user=user)
            token.save()
            print("Use this token to configure the Blackhole Client:\n")
            print(f"Token: {token}\n")
        else:
            print("The user is already created")

