from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path("ws/session_stream/<str:session_uuid>/", consumers.SessionStreamConsumer),
    path("ws/chat/<str:room_name>/", consumers.ChatConsumer),
]