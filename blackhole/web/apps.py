import logging
import sys
from django.conf import settings
from django.apps import AppConfig
from .license import License

logger = logging.getLogger(__name__)


class WebConfig(AppConfig):
    name = 'web'
    license = None

    # def ready(self):
    #     if not self.license:
    #         try:
    #             self.license = License(settings.CLIENT_SECRET_FILE, settings.CLIENT_KEY_FILE)
    #         except:
    #             sys.exit(1)