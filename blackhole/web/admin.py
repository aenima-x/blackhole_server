# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Environment, SecureShell, PrivateKey, Database, SecureShellConnection, \
    DatabaseConnection, Profile, AppUser, SessionRecord #RemoteDesktop, RDPConnection



@admin.register(Environment)
class EnvironmentAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name',)


@admin.register(SecureShell)
class SecureShellAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'environment',
        'ip',
        'port',
        'device_type',
    )
    list_filter = ('environment',)
    search_fields = ('name',)


@admin.register(PrivateKey)
class PrivateKeyAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'environment',
        'key_type'
    )
    list_filter = ('user', 'environment')


@admin.register(Database)
class DatabaseAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'environment',
        'ip',
        'port',
        'engine',
    )
    list_filter = ('environment',)
    search_fields = ('name',)


@admin.register(SecureShellConnection)
class SecureShellConnectionAdmin(admin.ModelAdmin):
    list_display = (
        'target',
        'authentication_method',
        'user'
    )
    list_filter = ('target', 'user')


@admin.register(DatabaseConnection)
class DataBaseConnectionAdmin(admin.ModelAdmin):
    list_display = ('target', 'user')
    list_filter = ('target', 'user')


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    filter_horizontal = ('secure_shell_connections', 'database_connections')


@admin.register(AppUser)
class AppUserAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'first_name',
        'last_name',
        'profile',
        'enabled',
        'time_range_enabled',
        'manual_ssh',
        'last_login',
    )
    list_filter = (
        'profile',
        'enabled',
        'time_range_enabled',
        'manual_ssh',
        'last_login',
    )
    filter_horizontal = ('not_allowed_environments',)

@admin.register(SessionRecord)
class SessionRecordAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'target',
        'target_user',
        'login_date',
        'connection_type',
    )
    list_filter = ('username', 'target')
    search_fields = ['host', 'user__username', 'session_id']
    readonly_fields = ["session_id", "client_pid", "connection_type", "username", "target", "target_user", "source_ip",
                       "login_date", "logout_date", "log_file"]


# @admin.register(RemoteDesktop)
# class RemoteDesktopAdmin(admin.ModelAdmin):
#     list_display = (
#         'name',
#         'description',
#         'environment',
#         'ip',
#         'secure_rdp',
#     )
#     list_filter = ('environment', 'secure_rdp')
#     search_fields = ('name',)


# @admin.register(RDPConnection)
# class RDPConnectionAdmin(admin.ModelAdmin):
#     list_display = ('target', 'domain', 'user')
#     list_filter = ('target', 'user')