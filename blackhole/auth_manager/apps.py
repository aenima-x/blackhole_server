from django.apps import AppConfig


class WebConfig(AppConfig):
    name = 'auth_manager'
