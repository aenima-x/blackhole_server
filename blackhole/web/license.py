import logging
import os
from base64 import b64decode

from django.conf import settings
from django.utils import timezone
import jwt
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256

from .exceptions import LicenseError

logger = logging.getLogger(__name__)


class License(object):

    def __init__(self, client_secret_file, client_key_file):
        self.is_valid = False
        self.is_expired = True
        self.license_data = None
        if not os.path.isfile(client_secret_file):
            logger.error("License File: %s Missing", client_secret_file)
            raise LicenseError()
        if not os.path.isfile(client_key_file):
            logger.error("License File: %s Missing", client_key_file)
            raise LicenseError()
        else:
            try:
                with open(client_secret_file) as f:
                    self.client_secret = f.read().strip()
                with open(client_key_file) as f:
                    self.license_key = f.read().strip()
            except Exception as e:
                logger.error("Error reading license file: %s", e)
                raise LicenseError(e)
            else:
                try:
                    self.check_license()
                except Exception as e:
                    logger.error("Error checking license: %s", e)
                    raise LicenseError()
        logger.info("License loaded")

    def check_license(self):
        logger.info("Validating License")
        decoded_license = b64decode(self.license_key)
        logger.debug("Decode License JWT")
        license_jwt = jwt.decode(decoded_license, self.client_secret, algorithm='HS256')
        logger.debug("Decode Data JWT")
        license_data = jwt.decode(license_jwt['jwt'], self.client_secret, algorithm='HS256')
        pub = RSA.importKey(settings.PUBLIC_KEY)
        hash = SHA256.new(license_jwt['jwt'].encode('utf-8')).digest()
        logger.debug("Validating Signature with public Key")
        if pub.verify(hash, license_jwt['signature']):
            self.license_data = license_data
            self.is_valid = True
            valid_date = (timezone.datetime.strptime(license_data['valid_from'], "%Y-%m-%d") + timezone.timedelta(
                days=license_data['valid_days'])).date()
            if timezone.now().date() < valid_date:
                self.is_expired = False






