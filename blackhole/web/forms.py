from django import forms
from django.forms import DateInput
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User
from .models import AppUser, Profile, SecureShellConnection, Environment, DatabaseConnection, Target #RDPConnection


class AppUserForm(forms.ModelForm):
    class Meta:
        model = AppUser
        exclude = ('time_range_enabled', 'time_range_enabled_since', 'time_range_enabled_to',
                   'not_allowed_environments', 'last_login')


class AppUserUpdateForm(forms.ModelForm):
    class Meta:
        model = AppUser
        exclude = ('last_login',)


class AdminUserForm(UserChangeForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'is_active', 'groups', 'user_permissions', 'password']


class SecureShellConnectionForm(forms.ModelForm):

    class Meta:
        model = SecureShellConnection
        widgets = {
            'password': forms.PasswordInput()
        }
        fields = '__all__'

class DataBaseConnectionForm(forms.ModelForm):

    class Meta:
        model = DatabaseConnection
        widgets = {
            'password': forms.PasswordInput()
        }
        fields = '__all__'

# class RDPConnectionForm(forms.ModelForm):
#
#     class Meta:
#         model = RDPConnection
#         widgets = {
#             'password': forms.PasswordInput()
#         }
#         fields = '__all__'


class FindSessionLogsForm(forms.Form):
    target = forms.ModelChoiceField(queryset=Target.objects.all(), label=_('Target'), required=False)
    session_id = forms.CharField(label=_('Session Id'), required=False)
    user = forms.ModelChoiceField(queryset=AppUser.objects.all(), label=_('User'), required=False)
    from_date = forms.DateField(label=_('From'), widget=DateInput(format='%d/%m/%Y', attrs={'class': 'datePicker'}), input_formats=('%d/%m/%Y',))
    to_date = forms.DateField(label=_('To'), widget=DateInput(format='%d/%m/%Y', attrs={'class': 'datePicker'}), input_formats=('%d/%m/%Y',))