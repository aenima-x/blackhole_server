# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib.auth import views as django_views
from django.contrib.auth.decorators import login_required
from .views import PasswordManagementIndex, GenerateRecoverLink, GenerateResetLink, ProcessRequest


urlpatterns = patterns('',
                       url(r'^$', PasswordManagementIndex.as_view(), name='passsword_management'),
                       url(r'^recover/$', GenerateRecoverLink.as_view(), name='get_recover_link'),
                       url(r'^reset/$', GenerateResetLink.as_view(), name='get_reset_link'),
                       url(r'^process_request/$', ProcessRequest.as_view(), name='process_request'),
                       # url(r'^$', home, name='launcher_index'),
                       # url(r'^links/$', login_required(Links.as_view()),
                       #     name='links'),
                       # url(r'add_favorite/(?P<web_pk>\d+)/$', add_favorite, name='add_favorite'),
                       # url(r'remove_favorite/(?P<web_pk>\d+)/$', remove_favorite, name='remove_favorite'),
                       # #url(r'^selectable/', include('selectable.urls')),
)