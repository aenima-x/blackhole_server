import logging
import asyncio
import json
from django.contrib.auth import get_user_model
from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer

logger = logging.getLogger(__name__)

class SessionStreamConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.session_uuid = self.scope['url_route']['kwargs']['session_uuid']
        logger.info("Connected To Session Stream: %s", self.session_uuid)
        self.session_name = f"sessions.{self.session_uuid}"
        await self.channel_layer.group_add(
            self.session_name,
            "channel"
        )
        await self.accept()

    async def receive(self, text_data):
        logger.info("Receive %s", text_data)

    async def disconnect(self, close_code):
        logger.info("Disconnected %s", close_code)

    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))