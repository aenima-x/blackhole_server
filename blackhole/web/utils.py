from base64 import b64decode

import logging
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from django.conf import settings
from django.utils.encoding import force_text
from django.utils.text import get_text_list
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
import jwt

logger = logging.getLogger("blackhole.utils")

def construct_change_message(request, form, formsets, add=False):
    """
    Construct a change message from a changed object.
    """
    change_message = []
    if add:
        change_message.append(_('Added.'))
    elif form.changed_data:
        change_message.append(_('Changed %s.') % get_text_list(form.changed_data, _('and')))

    if formsets:
        for formset in formsets:
            for added_object in formset.new_objects:
                change_message.append(_('Added %(name)s "%(object)s".')
                                      % {'name': force_text(added_object._meta.verbose_name),
                                         'object': force_text(added_object)})
            for changed_object, changed_fields in formset.changed_objects:
                change_message.append(_('Changed %(list)s for %(name)s "%(object)s".')
                                      % {'list': get_text_list(changed_fields, _('and')),
                                         'name': force_text(changed_object._meta.verbose_name),
                                         'object': force_text(changed_object)})
            for deleted_object in formset.deleted_objects:
                change_message.append(_('Deleted %(name)s "%(object)s".')
                                      % {'name': force_text(deleted_object._meta.verbose_name),
                                         'object': force_text(deleted_object)})
    change_message = ' '.join(change_message)
    return change_message or _('No fields changed.')


def get_content_type_for_model(obj):
    # Since this module gets imported in the application's root package,
    # it cannot import models from other applications at the module level.
    from django.contrib.contenttypes.models import ContentType
    return ContentType.objects.get_for_model(obj, for_concrete_model=False)


def log_addition(request, object, message):
    """
    Log that an object has been successfully added.
    The default implementation creates an admin LogEntry object.
    """
    from django.contrib.admin.models import LogEntry, ADDITION
    LogEntry.objects.log_action(
            user_id=request.user.pk,
            content_type_id=get_content_type_for_model(object).pk,
            object_id=object.pk,
            object_repr=force_text(object),
            action_flag=ADDITION,
            change_message=message,
    )


def log_change(request, object, message):
    """
    Log that an object has been successfully changed.
    The default implementation creates an admin LogEntry object.
    """
    from django.contrib.admin.models import LogEntry, CHANGE
    LogEntry.objects.log_action(
            user_id=request.user.pk,
            content_type_id=get_content_type_for_model(object).pk,
            object_id=object.pk,
            object_repr=force_text(object),
            action_flag=CHANGE,
            change_message=message,
    )


def log_deletion(request, object):
    """
    Log that an object will be deleted. Note that this method must be
    called before the deletion.
    The default implementation creates an admin LogEntry object.
    """
    from django.contrib.admin.models import LogEntry, DELETION
    LogEntry.objects.log_action(
            user_id=request.user.pk,
            content_type_id=get_content_type_for_model(object).pk,
            object_id=object.pk,
            object_repr=force_text(object),
            action_flag=DELETION,
    )

