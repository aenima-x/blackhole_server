# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.conf import settings
from django.views.generic import TemplateView
from django.utils import timezone
from braces.views import JSONResponseMixin
from django.views.generic import View
from .models import LocalUser
from .forms import RecoverPasswordForm, ResetPasswordForm
import pickle
import time
import base64
import random, string
import re
from django.core.mail import send_mail

RECOVER_OPERATION = 'recover'
RESET_OPERATION = 'reset'

class PasswordManagementIndex(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(PasswordManagementIndex, self).get_context_data(**kwargs)
        context['reset_password_form'] = ResetPasswordForm()
        context['recover_password_form'] = RecoverPasswordForm()
        return context

def send_recover_link(local_user):
    now = timezone.now()
    data = {'id': local_user.id, 'username': local_user.username, 'ts': time.mktime(now.timetuple()), 'operation': RECOVER_OPERATION}
    encoded = base64.b64encode(pickle.dumps(data))
    url = "{0}{1}".format(settings.WEB_URL, "{0}?token={1}".format(reverse('process_request'), encoded))
    message = u"""Para obtener su nueva contraseña ingrese en este link / To get a new password enter in this link <a href="%s">Obtener Password / Get Password</a>
    <br>El link expira en 5 minutos / The link expires in 5 minutes.""" % url
    send_mail('TGT - Recupero de Contraseña / Get Password','', 'Dont Reply <gestion_passwords@tgtarg.com>',
    [local_user.email], fail_silently=False, html_message=message)

def send_reset_link(local_user, password):
    now = timezone.now()
    data = {'id': local_user.id, 'username': local_user.username, 'ts': time.mktime(now.timetuple()), 'operation': RESET_OPERATION, 'password': password}
    encoded = base64.b64encode(pickle.dumps(data))
    url = "{0}{1}".format(settings.WEB_URL, "{0}?token={1}".format(reverse('process_request'), encoded))
    message = u"""Para cambiar su contraseña ingrese en este line / To change your password enter in this link <a href="%s">Cambiar Password / Change Password</a>
    <br>El link expira en 5 minutos.""" % url
    send_mail('TGT -  Cambio de Contraseña / Change Password','', 'Dont Reply <gestion_passwords@tgtarg.com>',
    [local_user.email], fail_silently=False, html_message=message)

class GenerateRecoverLink(JSONResponseMixin, View):

    def get(self, request, *args, **kwargs):
        username = request.GET.get('username', False)
        email = request.GET.get('mail', False)
        message = ''
        error = False
        try:
            local_user = LocalUser.objects.get(username=username, email=email)
            # genero el link y lo mando por mail
            message = u'El link para el recupero fue enviado al usuario / The recover link was sent to the user.'
            try:
                send_recover_link(local_user)
            except Exception as e:
                print e
        except LocalUser.DoesNotExist:
            message = u"El usuario/mail ingresado no existe! / User/email invalid"
            error = True
        context_dict = {
            u"message": message,
            u'error': error
        }

        return self.render_json_response(context_dict)

class GenerateResetLink(JSONResponseMixin, View):

    def get(self, request, *args, **kwargs):
        username = request.GET.get('username', False)
        password = request.GET.get('password', False)
        new_password = request.GET.get('new_password', False)
        message = ''
        error = False
        try:
            local_user = LocalUser.objects.get(username=username)
            if password == local_user.password:
                if new_password != password:
                    message = u'El link para el reseteo fue enviado al usuario. / The reset link was sent to the user'
                    send_reset_link(local_user, new_password)
                else:
                    message = u'El nuevo password no puede ser igual al anterior! / You can\'t repeat the last password'
            else:
                message = u'Usuario o password incorrectos! / Invalid username or password'
        except LocalUser.DoesNotExist:
            message = u"El usuario ingresado no existe!"
            error = True
        context_dict = {
            u"message": message,
            u'error': error
        }

        return self.render_json_response(context_dict)


class ProcessRequest(TemplateView):
    template_name = "process_request.html"

    def get_context_data(self, **kwargs):
        context = super(ProcessRequest, self).get_context_data(**kwargs)
        token = self.request.GET.get('token', False)
        if not token:
            context['error'] = "URL Invalida"
        else:
            data = pickle.loads(base64.b64decode(token))
            if set(('username', 'id', 'ts', 'operation')).issubset(data):
                operation = data['operation']
                date = data['ts']
                now = time.mktime(timezone.now().timetuple())
                delta = (now - date) / 60
                username = data['username']
                user_id = data['id']
                if delta < settings.WEB_RESET_EXPIRE_MINUTES:
                    if operation == RECOVER_OPERATION:
                        try:
                            local_user = LocalUser.objects.get(username=username, id=user_id)
                            rnd = random.SystemRandom()
                            chars = [rnd.choice(string.ascii_letters) for i in range(rnd.randint(6,10))] + [rnd.choice(string.digits) for i in range(rnd.randint(2,4))] + [rnd.choice('!@#$%^&*()_-') for i in range(rnd.randint(1,3))]
                            rnd.shuffle(chars)
                            password = ''.join(chars)
                            local_user.password = password
                            local_user.password_last_change = timezone.now().date()
                            local_user.save()
                            context['message'] = u'El password del usuario / the password for the user <strong>%s</strong>: %s' % (local_user.username, password)
                            # Genero un pasword random y se lo pongo
                        except LocalUser.DoesNotExist:
                            context['error'] = u"URL Invalida"
                    elif operation == RESET_OPERATION:
                        password = data.get('password', False)
                        if password:
                            r = re.compile(settings.PASSWORD_REGEX)
                            if r.match(password):
                                try:
                                    local_user = LocalUser.objects.get(username=username, id=user_id)
                                    if local_user.password == password:
                                        context['error'] = u"El password no puede ser igual al anterior / You can't repeat your last password"
                                    else:
                                        local_user.password = password
                                        local_user.password_last_change = timezone.now().date()
                                        local_user.save()
                                        context['message'] = u'El password del usuario / the password for the user <strong>%s</strong>: %s' % (local_user.username, password)
                                    # Genero un pasword random y se lo pongo
                                except LocalUser.DoesNotExist:
                                    context['error'] = u"URL Invalida"
                            else:
                                context['error'] = u"URL Invalida"
                        else:
                            context['error'] = u"URL Invalida"
                    else:
                        context['error'] = u"URL Invalida"
                else:
                    context['error'] = u"URL Expirada"
            else:
                context['error'] = u"URL Invalida"

        return context