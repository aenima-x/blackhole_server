Install
------

```shell
useradd -s /bin/bash -d /home/blackhole -m -g blackhole blackhole
sudo apt install python3-dev libldap2-dev libssl-dev libsasl2-dev redis mysql-server libmysqlclient-dev 
cp example.env blackhole/blackhole/.env

SECRET_KEY=`python3 -c "import random, string;print(''.join([random.SystemRandom().choice(f\"{string.ascii_letters}{string.digits}{'#%&:;_-+*$@'}\") for i in range(50)]))"` && sed -i "s/__SECRET_KEY__/$SECRET_KEY/" blackhole/blackhole/.env
FIELD_ENCRYPTION_KEY=`python3 -c "import base64, os;print(base64.urlsafe_b64encode(os.urandom(32)).decode())"` && sed -i "s/__FIELD_ENCRYPTION_KEY__/$FIELD_ENCRYPTION_KEY/" blackhole/blackhole/.env
mkdir /etc/blackhole/
mkdir /var/log/blackhole/
```

###### Create te database
```
CREATE DATABASE blackhole;
CREATE USER 'blackhole'@'localhost' IDENTIFIED BY 'PASSWORD';
GRANT ALL PRIVILEGES ON blackhole.* TO 'blackhole'@'localhost';
```
Add the credentials to ```blackhole/blackhole/.env```

###### Create data and config
```
./manage.py migrate
./manage.py createsuperuser
./manage.py initial_setup
```

