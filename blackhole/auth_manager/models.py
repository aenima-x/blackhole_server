from django.core.validators import RegexValidator
from django.db import models
from encrypted_model_fields.fields import EncryptedCharField
from django.conf import settings
# Create your models here.


class LocalUser(models.Model):
    username = models.CharField(verbose_name="Username", unique=True, max_length=50)
    name = models.CharField(max_length=50, verbose_name="Name")
    last_name = models.CharField(max_length=50, verbose_name="Last Name")
    enabled = models.BooleanField(default=True, verbose_name="Enabled")
    #email = models.EmailField(verbose_name=u"Email", unique=True)
    password = EncryptedCharField(max_length=100, verbose_name="Password",
                                  validators=[RegexValidator(regex=settings.PASSWORD_REGEX,
                                                             message="The password must include at least 8 characters (letters, numbers and special characters)")])
    password_last_change = models.DateField(auto_now_add=True, verbose_name="Password last change")
    #expires = models.BooleanField(default=True, verbose_name="Password Expires")

    def __str__(self):
        return f"User: {self.username}"


class ActiveDirectory(models.Model):
    name = models.CharField(max_length=50, verbose_name="Name")
    domain_controller_ip = models.GenericIPAddressField(protocol="IPv4", verbose_name="Domain Controller")
    domain_controller_port = models.IntegerField(default=389, verbose_name="Port")
    secure = models.BooleanField(default=False, verbose_name="Secure")

    def __str__(self):
        return f"AD: {self.name}"

    def get_domain_controller(self):
        return f"ldap://{self.domain_controller_ip}:{self.domain_controller_port}"

    class Meta(object):
        verbose_name = "Active Directory"
        verbose_name_plural = "Active Directories"

class RadiusClient(models.Model):
    name = models.CharField(max_length=50, verbose_name="Name")
    enabled = models.BooleanField(default=True, verbose_name="Enabled")
    address = models.GenericIPAddressField(protocol="IPv4",verbose_name="IP Address", unique=True)
    secret = EncryptedCharField(max_length=100, verbose_name="Secret")
    valida_local_users = models.BooleanField(default=True, verbose_name="Validate Local Users")
    domains = models.ManyToManyField(ActiveDirectory, verbose_name="Validate Domains", blank=True)

    def __str__(self):
        return f"Radius Client: {self.name}"
