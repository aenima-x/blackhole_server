from web.models import AppUser, Profile, SecureShellConnection, SecureShell, Environment, Database, \
    DatabaseConnection, PrivateKey, SessionRecord
from rest_framework import serializers


class ChoiceField(serializers.ChoiceField):

    def to_representation(self, obj):
        return self._choices[obj]


class EnvironmentSerializer(serializers.ModelSerializer):

    def validate_name(self, attrs):
        return attrs

    class Meta:
        model = Environment
        fields = ("name", )


class PrivateKeySerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer()
    key_type = ChoiceField(choices=PrivateKey.TYPE_CHOICES)
    class Meta:
        model = PrivateKey
        fields = "__all__"


class FullSecureShellSerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer()
    device_type = ChoiceField(choices=SecureShell.DEVICE_TYPE_CHOICES)

    class Meta:
        model = SecureShell
        fields = ("id", "name", "ip", "port", "description", "environment", "device_type")


class FullSecureShellConnectionSerializer(serializers.ModelSerializer):
    target = FullSecureShellSerializer()
    authentication_method = ChoiceField(choices=SecureShellConnection.AUTHENTICATION_METHODS_CHOICES)

    class Meta:
        model = SecureShellConnection
        fields = ("id", "target", "authentication_method", "user", "password")


class SimpleSecureShellSerializer(serializers.ModelSerializer):
    environment = serializers.CharField(max_length=35, required=True)
    auth_user = serializers.CharField(max_length=30, required=True)
    profile = serializers.CharField(max_length=15, required=True)

    def validate_ip(self, value):
        if SecureShell.objects.filter(ip=value).exists():
            raise serializers.ValidationError(f'A host with the ip {value} already exists')
        else:
            return value

    def validate_profile(self, value):
        if not Profile.objects.filter(name=value).exists():
            raise serializers.ValidationError(f"Profile {value} don't exists")
        else:
            return value

    class Meta:
        model = SecureShell
        fields = ("id", "name", "description", "ip", "environment", "port", "auth_user", "profile")

    def create(self, validated_data):
        environment_name = validated_data.pop('environment')
        environment, created = Environment.objects.get_or_create(name=environment_name)
        auth_user = validated_data.pop('auth_user')
        profile_name = validated_data.pop('profile')
        if created:
            environment.save()
        secureshell = SecureShell(environment=environment, **validated_data)
        secureshell.save()
        secureshell_connection = SecureShellConnection(target=secureshell, user=auth_user)
        secureshell_connection.save()
        profile = Profile.objects.get(name=profile_name)
        profile.secure_shell_connections.add(secureshell_connection)
        profile.save()
        secureshell.auth_user = auth_user
        secureshell.profile = profile_name
        return secureshell


class SimpleSecureShellConnectionSerializer(serializers.ModelSerializer):
    target = SimpleSecureShellSerializer()

    class Meta:
        model = SecureShellConnection
        fields = ("id", "target", "user")


class ProfileSerializer(serializers.ModelSerializer):
    secure_shell_connections = SimpleSecureShellConnectionSerializer(many=True)

    class Meta:
        model = Profile
        fields = "__all__"