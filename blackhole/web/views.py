import os
import logging
import datetime
import json
from io import StringIO

from dal import autocomplete
import redis
from paramiko import RSAKey, DSSKey
from ansi2html import Ansi2HTMLConverter
from django.conf import settings
from django.apps import apps
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import TemplateView, CreateView, ListView, DeleteView, UpdateView, DetailView, FormView
from django.db.models import Q
from django.views.generic.edit import FormMixin

from .utils import log_addition, log_change, log_deletion, construct_change_message
from .forms import AppUserForm, AppUserUpdateForm, SecureShellConnectionForm, AdminUserForm, FindSessionLogsForm, DataBaseConnectionForm
from .models import AppUser, Profile, SecureShell, Environment, PrivateKey, SecureShellConnection, SessionRecord, \
    Database, DatabaseConnection



logger = logging.getLogger(__name__)

LOG_SIZE_LIMIT = 2000


class IndexTemplateView(LoginRequiredMixin, TemplateView):
    template_name = "web/index.html"



## Admin
class AdminUserCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = User
    template_name = "web/admin_user_form.html"
    form_class = UserCreationForm
    success_url = reverse_lazy('home')
    success_message = _("%(username)s was created successfully")
    permission_required = ('auth.add_user',)

    def get_success_url(self):
        log_addition(self.request, self.object, _('Added.'))
        return self.success_url


class AdminUserListView(PermissionRequiredMixin, ListView):
    model = User
    template_name = "web/admin_user_list.html"
    paginate_by = 30
    permission_required = ('auth.view_user',)


class AdminUserUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    form_class = AdminUserForm
    template_name = "web/admin_user_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(username)s was updated successfully")
    permission_required = ('auth.change_user',)

    def form_valid(self, form):
        change_message = construct_change_message(self.request, form, None)
        log_change(self.request, self.object, change_message)
        return super().form_valid(form)

class AdminUserDeleteView(PermissionRequiredMixin, DeleteView):
    model = User
    success_url = reverse_lazy('admin_user_list')
    template_name = "web/admin_user_confirm_delete.html"
    permission_required = ('auth.delete_user',)

class GroupCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Group
    template_name = "web/group_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was created successfully")
    fields = '__all__'
    permission_required = ('auth.add_group',)


class GroupUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Group
    template_name = "web/group_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was updated successfully")
    fields = '__all__'
    permission_required = ('auth.change_group',)


class GroupListView(PermissionRequiredMixin, ListView):
    model = Group
    template_name = "web/group_list.html"
    paginate_by = 30
    permission_required = ('auth.view_group',)


class GroupDeleteView(PermissionRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('group_list')
    template_name = "web/group_confirm_delete.html"
    permission_required = ('auth.delete_group',)



## AppUser
class AppUserCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = AppUser
    template_name = "web/app_user_form.html"
    form_class = AppUserForm
    success_url = reverse_lazy('home')
    success_message = _("%(username)s was created successfully")
    permission_required = ('web.add_appuser',)

    def get_success_url(self):
        log_addition(self.request, self.object, _('Added.'))
        return self.success_url


class AppUserListView(PermissionRequiredMixin, ListView):
    model = AppUser
    template_name = "web/app_user_list.html"
    paginate_by = 30
    permission_required = ('web.view_appuser',)


class AppUserUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = AppUser
    form_class = AppUserUpdateForm
    template_name = "web/app_user_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(username)s was updated successfully")
    permission_required = ('web.change_appuser',)

    def form_valid(self, form):
        change_message = construct_change_message(self.request, form, None)
        log_change(self.request, self.object, change_message)
        return super(AppUserUpdateView, self).form_valid(form)


class AppUserDeleteView(PermissionRequiredMixin, DeleteView):
    model = AppUser
    success_url = reverse_lazy('app_user_list')
    template_name = "web/app_user_confirm_delete.html"
    permission_required = ('web.delete_appuser',)

    def get_success_url(self):
        log_deletion(self.request, self.object)
        return self.success_url


## Profile
class ProfileCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Profile
    template_name = "web/profile_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was created successfully")
    fields = '__all__'
    permission_required = ('web.add_profile',)

    def get_success_url(self):
        log_addition(self.request, self.object, _('Added.'))
        return self.success_url


class ProfileUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Profile
    template_name = "web/profile_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was updated successfully")
    fields = '__all__'
    permission_required = ('web.change_profile',)

    def form_valid(self, form):
        change_message = construct_change_message(self.request, form, None)
        log_change(self.request, self.object, change_message)
        return super(ProfileUpdateView, self).form_valid(form)


class ProfileListView(PermissionRequiredMixin, ListView):
    model = Profile
    template_name = "web/profile_list.html"
    paginate_by = 30
    permission_required = ('web.view_profile',)


class ProfileDeleteView(PermissionRequiredMixin, DeleteView):
    model = Profile
    success_url = reverse_lazy('profile_list')
    template_name = "web/profile_confirm_delete.html"
    permission_required = ('web.delete_profile',)

    def get_success_url(self):
        log_deletion(self.request, self.object)
        return self.success_url


## SecureShell
class SecureShellCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = SecureShell
    template_name = "web/secure_shell_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was created successfully")
    fields = '__all__'
    permission_required = ('web.add_secureshell',)

    def get_success_url(self):
        log_addition(self.request, self.object, _('Added.'))
        return self.success_url


class SecureShellUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = SecureShell
    template_name = "web/secure_shell_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was updated successfully")
    fields = '__all__'
    permission_required = ('web.change_secureshell',)


    def form_valid(self, form):
        change_message = construct_change_message(self.request, form, None)
        log_change(self.request, self.object, change_message)
        return super().form_valid(form)


class SecureShellListView(PermissionRequiredMixin, ListView):
    model = SecureShell
    template_name = "web/secure_shell_list.html"
    paginate_by = 30
    permission_required = ('web.view_secureshell',)


class SecureShellDeleteView(PermissionRequiredMixin, DeleteView):
    model = SecureShell
    success_url = reverse_lazy('secure_shell_list')
    template_name = "web/secure_shell_confirm_delete.html"
    permission_required = ('web.delete_secureshell'),

    def get_success_url(self):
        log_deletion(self.request, self.object)
        return self.success_url


# ## Databases
class DataBaseCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Database
    template_name = "web/database_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was created successfully")
    fields = '__all__'
    permission_required = ('web.add_database',)

    def get_success_url(self):
        log_addition(self.request, self.object, _('Added.'))
        return self.success_url


class DatabaseUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Database
    template_name = "web/database_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was updated successfully")
    fields = '__all__'
    permission_required = ('web.change_database',)

    def form_valid(self, form):
        change_message = construct_change_message(self.request, form, None)
        log_change(self.request, self.object, change_message)
        return super().form_valid(form)


class DatabaseListView(PermissionRequiredMixin, ListView):
    model = Database
    template_name = "web/database_list.html"
    paginate_by = 30
    permission_required = ('web.view_database',)


class DatabaseDeleteView(PermissionRequiredMixin, DeleteView):
    model = Database
    success_url = reverse_lazy('database_list')
    template_name = "web/database_confirm_delete.html"
    permission_required = ('web.delete_database',)

    def get_success_url(self):
        log_deletion(self.request, self.object)
        return self.success_url



# Environment
class EnvironmentCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Environment
    template_name = "web/environment_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was created successfully")
    fields = '__all__'
    permission_required = ('web.add_environment',)

    def get_success_url(self):
        log_addition(self.request, self.object, _('Added.'))
        return self.success_url


class EnvironmentUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Environment
    template_name = "web/environment_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("%(name)s was updated successfully")
    fields = '__all__'
    permission_required = ('web.change_environment',)

    def form_valid(self, form):
        change_message = construct_change_message(self.request, form, None)
        log_change(self.request, self.object, change_message)
        return super(EnvironmentUpdateView, self).form_valid(form)


class EnvironmentListView(PermissionRequiredMixin, ListView):
    model = Environment
    template_name = "web/environment_list.html"
    paginate_by = 30
    permission_required = ('web.view_environment',)


class EnvironmentDeleteView(PermissionRequiredMixin, DeleteView):
    model = Environment
    success_url = reverse_lazy('environment_list')
    template_name = "web/environment_confirm_delete.html"
    permission_required = ('web.delete_environment', )

    def get_success_url(self):
        log_deletion(self.request, self.object)
        return self.success_url


# Private Key

class PrivateKeyCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = PrivateKey
    template_name = "web/privatekey_form.html"
    success_url = reverse_lazy('home')
    success_message = _("PrivateKey for %(user)s was created successfully")
    fields = '__all__'
    permission_required = ('web.add_privatekey',)

    def get_success_url(self):
        log_addition(self.request, self.object, _('Added.'))
        return self.success_url


class PrivateKeyUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PrivateKey
    template_name = "web/privatekey_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("PrivateKey for %(user)s was updated successfully")
    fields = '__all__'
    permission_required = ('web.change_privatekey',)

    def form_valid(self, form):
        change_message = construct_change_message(self.request, form, None)
        log_change(self.request, self.object, change_message)
        return super(PrivateKeyUpdateView, self).form_valid(form)


class PrivateKeyListView(PermissionRequiredMixin, ListView):
    model = PrivateKey
    template_name = "web/privatekey_list.html"
    paginate_by = 30
    permission_required = ('web.view_privatekey',)


class PrivateKeyDeleteView(PermissionRequiredMixin, DeleteView):
    model = PrivateKey
    success_url = reverse_lazy('privatekey_list')
    template_name = "web/privatekey_confirm_delete.html"
    permission_required = ('web.delete_privatekey',)

    def get_success_url(self):
        log_deletion(self.request, self.object)
        return self.success_url

# Secure Shell Connection

class SecureShellConnectionCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = SecureShellConnection
    template_name = "web/secure_shell_connection_form.html"
    success_url = reverse_lazy('home')
    form_class = SecureShellConnectionForm
    success_message = _("SecureShell Connection for %(user)s to %(target)s was created successfully")
    permission_required = ('web.add_secureshellconnection',)


class SecureShellConnectionUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = SecureShellConnection
    template_name = "web/secure_shell_connection_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _("SecureShell Connection for %(user)s to %(target)s was updated successfully")
    fields = '__all__'
    permission_required = ('web.change_secureshellconnection',)


class SecureShellConnectionListView(PermissionRequiredMixin, ListView):
    model = SecureShellConnection
    template_name = "web/secure_shell_connection_list.html"
    paginate_by = 30
    permission_required = ('web.view_secureshellconnection',)


class SecureShellConnectionDeleteView(PermissionRequiredMixin, DeleteView):
    model = SecureShellConnection
    success_url = reverse_lazy('secure_shell_connection_list')
    template_name = "web/secure_shell_connection_confirm_delete.html"
    permission_required = ('web.delete_secureshellconnection',)


# Database Connection
class DataBaseConnectionCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = DatabaseConnection
    template_name = "web/database_connection_form.html"
    success_url = reverse_lazy('home')
    form_class = DataBaseConnectionForm
    success_message = _(u"Database Connection for %(user)s to %(target)s was created successfully")
    permission_required = ('web.add_databaseconnection')


class DataBaseConnectionUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = DatabaseConnection
    template_name = "web/database_connection_update_form.html"
    success_url = reverse_lazy('home')
    success_message = _(u"Database Connection for %(user)s to %(target)s was updated successfully")
    fields = '__all__'
    permission_required = ('web.change_databaseconnection',)


class DataBaseConnectionListView(PermissionRequiredMixin, ListView):
    model = DatabaseConnection
    template_name = "web/database_connection_list.html"
    paginate_by = 30
    permission_required = ('web.view_databaseconnection',)

class DataBaseConnectionDeleteView(PermissionRequiredMixin, DeleteView):
    model = DatabaseConnection
    success_url = reverse_lazy('database_connection_list')
    template_name = "web/database_connection_confirm_delete.html"
    permission_required = ('web.delete_databaseconnection',)


class FindSessionRecordView(FormMixin, ListView):
    model = SessionRecord
    form_class = FindSessionLogsForm
    template_name = 'web/find_session_log.html'

    def get(self, request, *args, **kwargs):
        # From ProcessFormMixin
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)

        # From BaseListView
        self.object_list = self.get_queryset()

        context = self.get_context_data(object_list=self.object_list, form=self.form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def get_queryset(self):
        form = self.form_class(self.request.POST)
        if form.is_valid():
            target = form.cleaned_data['target']
            session_id = form.cleaned_data['session_id']
            user = form.cleaned_data['user']
            from_date = form.cleaned_data['from_date']
            to_date = form.cleaned_data['to_date']
            days = datetime.timedelta(days=1)
            query = SessionRecord.objects.filter(login_date__range=(from_date, to_date + days))
            if target:
                query = query.filter(target=target.name)
            if session_id:
                query = query.filter(session_id=session_id)
            if user:
                query = query.filter(username=user.username)
            for log in query:
                if log.log_file:
                    if os.path.isfile(log.log_file):
                        log.can_download_log = True
                    else:
                        log.can_download_log = False
                else:
                    log.can_download_log = False
            return query
        else:
            return None

# Autocomplete


class DataBaseConnectionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = DatabaseConnection.objects.all()

        if self.q:
            qs = qs.filter(target__name__icontains=self.q).order_by('target')
        return qs


class SecureShellConnectionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = SecureShellConnection.objects.all()

        if self.q:
            qs = qs.filter(target__name__icontains=self.q).order_by('target')
        return qs


class PrivateKeyAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = PrivateKey.objects.all()

        if self.q:
            subquery = (
            Q(user__username__icontains=self.q) | Q(environment__name__icontains=self.q))
            qs = qs.filter(subquery).order_by('user')
        return qs


class ProfileAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = Profile.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q).order_by('name')
        return qs


class AppUserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = AppUser.objects.all()

        if self.q:
            subquery = (Q(username__icontains=self.q) | Q(first_name__icontains=self.q)| Q(last_name__icontains=self.q))
            qs = qs.filter(subquery).order_by('username')
        return qs


class EnvironmentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = Environment.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q).order_by('name')
        return qs


class SecureShellTargetAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = SecureShell.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q).order_by('name')
        return qs


class DatabaseTargetAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return Update.objects.none()

        qs = Database.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q).order_by('name')
        return qs


# web services
@permission_required('web.view_sessionrecord')
def kill_session(request):
    if request.is_ajax():
        session_id = int(request.POST['id'])
        logger.debug("Kill session ID: %s", session_id)
        try:
            session_log = SessionRecord.objects.get(pk=session_id)
            session_log.logout_date = timezone.now()
            session_log.save()
        except:
            pass
        redis_server = redis.Redis(settings.REDIS_SERVER)
        connections = redis_server.keys(settings.REDIS_CLIENTS_KEY % "*")
        for key in connections:
            dict_connection = json.loads(redis_server.get(key))
            redis_session_id = dict_connection['id']
            if session_id == redis_session_id:
                pid = dict_connection['client_pid']
                try:
                    redis_server.delete(key)
                    try:
                        logger.debug("KILLL PID: %s", pid)
                        os.kill(pid, 3)
                    except OSError as e:
                        logger.exception(e)
                    else:
                        data = "Session Killed"
                except Exception as e:
                    data = str(e)
                break
        return HttpResponse(data)

@permission_required('web.view_sessionrecord')
def get_sessions(request):
    if request.is_ajax():
        redis_server = redis.Redis(settings.REDIS_SERVER)
        session_keys = redis_server.keys(settings.REDIS_CLIENTS_KEY % "*")
        sessions = map(lambda x: json.loads(x), redis_server.mget(session_keys))
        json_data = json.dumps(sorted(sessions, key=lambda i: i['username']))
        return HttpResponse(json_data, content_type="application/json")

# # Template Views
class LicenseManagementView(PermissionRequiredMixin, TemplateView):
    template_name = "web/manage_license.html"
    permission_required = ('web.manage_license',)

    def get_context_data(self, **kwargs):
        license = apps.get_app_config('web').license
        valid_from = timezone.datetime.strptime(license.license_data['valid_from'], "%Y-%m-%d")
        valid_until = (valid_from + timezone.timedelta(days=license.license_data['valid_days'])).date()
        context = super(LicenseManagementView, self).get_context_data(**kwargs)
        context['valid_license'] = license.is_valid
        context['license_data'] = license.license_data
        context['valid_until'] = valid_until
        context['valid_from'] = valid_from.date()
        context['is_license_active'] = not license.is_expired
        context['created_targets_count'] = SecureShell.objects.all().count() + Database.objects.all().count()
        return context
#
class ActiveSessionTemplateView(TemplateView):
    template_name = "web/active_sessions.html"

    @method_decorator(permission_required('web.view_sessionrecord'))
    def dispatch(self, request, *args, **kwargs):
        return super(ActiveSessionTemplateView, self).dispatch(request, *args, **kwargs)


# Function View
@permission_required('web.view_sessionrecord')
def download_session_log(request, pk):
    session_record = get_object_or_404(SessionRecord, pk=pk)
    if os.path.isfile(session_record.log_file):
        log_basename = os.path.basename(session_record.log_file)
        conv = Ansi2HTMLConverter()
        with open(session_record.log_file) as f:
            html = conv.convert(f.read())
        html_filename = log_basename.replace(".log", ".html")
        s = StringIO()
        s.write(html)
        resp = HttpResponse(s.getvalue(), content_type="text/html")
        resp['Content-Disposition'] = 'attachment; filename=%s' % html_filename
        return resp
    else:
        raise Http404("Session record log missing")
#
#
# @permission_required('web.view_session_logs')
# def stream_session(request, pk):
#     session_log = SessionLog.objects.get(pk=pk)
#     conv = Ansi2HTMLConverter()
#     try:
#         with open(session_log.log_file) as f:
#             data = f.read()
#             if len(data) > LOG_SIZE_LIMIT:
#                 data = data[-LOG_SIZE_LIMIT:]
#         html = conv.convert(data, full=False)
#         resp = HttpResponse(html)
#         return resp
#     except IOError:
#         return HttpResponse("<h4>You can't watch this session</h4>")


def generate_key(request):
    key_type = int(request.GET.get('key_type'))
    user = request.GET.get('user')
    private_strem = StringIO()
    if key_type == PrivateKey.RSA_KEY_TYPE:
        priv = RSAKey.generate(2048)
        priv.write_private_key(private_strem)
        private_key = private_strem.getvalue().strip()
        private_strem.seek(0)
        pub = RSAKey(file_obj=private_strem)
        public_key = "%s %s %s@blackhole" % (pub.get_name(), pub.get_base64(), user)
    elif key_type == PrivateKey.DSA_KEY_TYPE:
        priv = DSSKey.generate(1024)
        priv.write_private_key(private_strem)
        private_key = private_strem.getvalue().strip()
        private_strem.seek(0)
        pub = DSSKey(file_obj=private_strem)
        public_key = "%s %s %s@blackhole" % (pub.get_name(), pub.get_base64(), user)
    key_dict = {'private': private_key, 'public': public_key}
    json_keys = json.dumps(key_dict)
    return  HttpResponse(json_keys, content_type='application/json')


def room(request, room_name):
    return render(request, 'web/chat_example.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    })