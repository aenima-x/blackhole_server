from django.contrib import admin
from .models import LocalUser, ActiveDirectory, RadiusClient
from django import forms
from django.forms.models import ModelForm


# Register your models here.

class PasswordForm(ModelForm):
    class Meta:
        widgets = {
            'password': forms.TextInput(attrs={'type': 'password'})
        }


@admin.register(LocalUser)
class LocalUserAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'name',
        'last_name',
        'enabled',
    )
    list_filter = ('enabled',)
    search_fields = ('name', 'last_name', 'username')
    form = PasswordForm


@admin.register(ActiveDirectory)
class ActiveDirectoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'domain_controller_ip',
        'domain_controller_port',
        'secure',
    )
    list_filter = ('secure',)
    search_fields = ('name',)


@admin.register(RadiusClient)
class RadiusClientAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'enabled',
        'address',
        'valida_local_users',
    )
    list_filter = ('enabled',)
    filter_horizontal = ('domains',)
    search_fields = ('name',)