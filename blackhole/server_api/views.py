import json
import logging
import time

import redis
from django.conf import settings
from rest_framework.decorators import action
from rest_framework.response import Response
from web.models import SecureShellConnection, PrivateKey, SecureShell
from rest_framework import status, viewsets, mixins

from .serializer import SimpleSecureShellSerializer
from .permissions import CustomPostPermission
from django.utils import timezone


logger = logging.getLogger(__name__)


class SecureShellViewSet(mixins.CreateModelMixin,
                        viewsets.GenericViewSet):
    queryset = SecureShell.objects.all()
    serializer_class = SimpleSecureShellSerializer
    permission_classes = (CustomPostPermission,)

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response

