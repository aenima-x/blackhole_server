import getpass
import os
from django.core.management.base import BaseCommand
from django.conf import settings
from pyrad.client import Client, Timeout
from pyrad.dictionary import Dictionary
import pyrad.packet

from auth_manager.models import RadiusClient

class Command(BaseCommand):
    help = "My shiny new management command."

    #def add_arguments(self, parser):
    #    parser.add_argument('username', nargs='*')

    def validate_user(self, radius_client, username, password):
        dictionary_file = os.path.join(settings.BASE_DIR, "auth_manager", "dictionary")
        try:
            client = Client(server='127.0.0.1',
                            secret=radius_client.secret.encode(),  # avoid UnicodeDecodeError
                            dict=Dictionary(dictionary_file),
                            )
        except AttributeError as e:
            self.stderr.write(f"Error: {e}")
        else:
            req = client.CreateAuthPacket(code=pyrad.packet.AccessRequest, User_Name=username)
            crypted_password = req.PwCrypt(password)
            req["User-Password"] = crypted_password

            try:
                self.stdout.write(f"Send {client} {req[2]}\n")
                reply = client.SendPacket(req)
            except Timeout as e:
                self.stderr.write(f"RADIUS Timeout contacting {e}\n")
            except Exception as e:
                self.stderr.write(f"RADIUS Unknown error sending {e}\n")
            else:
                self.stdout.write(f"Auth Manager Authenticate check reply.code={reply.code}\n")
                if reply.code == pyrad.packet.AccessReject:
                    print("RADIUS Reject username=%s" % username)
                else:
                    if reply.code == pyrad.packet.AccessAccept:
                        self.stderr.write("Auth Manager Accept OK")
                    else:
                        self.stderr.write(f"Auth Manager Unknown Code username={username} reply.code={reply.code}")

    def handle(self, *args, **options):
        try:
            radius_client = RadiusClient.objects.get(address='127.0.0.1', enabled=True)
        except RadiusClient.DoesNotExist:
            self.stderr.write("No local Radius Client Found")
        else:
            username = input("User: ")
            password = getpass.getpass("Password: ")
            self.validate_user(radius_client, username, password)
