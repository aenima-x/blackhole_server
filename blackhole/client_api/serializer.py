from web.models import AppUser, Profile, SecureShellConnection, SecureShell, Environment, Database, \
    DatabaseConnection, PrivateKey, SessionRecord #RDPConnection, RemoteDesktop
from rest_framework import serializers


class ChoiceField(serializers.ChoiceField):

    def to_representation(self, obj):
        return self._choices[obj]


class EnvironmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Environment
        fields = ("name", )


class PrivateKeySerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer()
    key_type = ChoiceField(choices=PrivateKey.TYPE_CHOICES)
    class Meta:
        model = PrivateKey
        fields = "__all__"


class FullDatabaseSerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer()
    engine = ChoiceField(choices=Database.ENGINE_CHOICES)

    class Meta:
        model = Database
        fields = ("id", "name", "ip", "port", "description", "environment", "engine")


class FullDatabaseConnectionSerializer(serializers.ModelSerializer):
    target = FullDatabaseSerializer()
    command = serializers.CharField(source="get_client_command")

    class Meta:
        model = DatabaseConnection
        fields = ("id", "target", "user", "command")


class FullSecureShellSerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer()
    device_type = ChoiceField(choices=SecureShell.DEVICE_TYPE_CHOICES)

    class Meta:
        model = SecureShell
        fields = ("id", "name", "ip", "port", "description", "environment", "device_type")


class FullSecureShellConnectionSerializer(serializers.ModelSerializer):
    target = FullSecureShellSerializer()
    authentication_method = ChoiceField(choices=SecureShellConnection.AUTHENTICATION_METHODS_CHOICES)

    class Meta:
        model = SecureShellConnection
        fields = ("id", "target", "authentication_method", "user", "password")


# class FullRDPSerializer(serializers.ModelSerializer):
#     environment = EnvironmentSerializer()
#
#     class Meta:
#         model = RemoteDesktop
#         fields = ("id", "name", "ip", "port", "description", "environment")


# class FullRDPConnectionSerializer(serializers.ModelSerializer):
#     target = FullRDPSerializer()
#
#     class Meta:
#         model = RDPConnection
#         fields = ("id", "target", "domain", "user", "password")


class SimpleDatabaseSerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer()

    class Meta:
        model = Database
        fields = ("id", "name", "description",  "ip", "environment")


class SimpleDatabaseConnectionSerializer(serializers.ModelSerializer):
    target = SimpleDatabaseSerializer()

    class Meta:
        model = DatabaseConnection
        fields = ("id", "target", "user")


class SimpleSecureShellSerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer()

    class Meta:
        model = SecureShell
        fields = ("id", "name", "description", "ip", "environment")


class SimpleSecureShellConnectionSerializer(serializers.ModelSerializer):
    target = SimpleSecureShellSerializer()

    class Meta:
        model = SecureShellConnection
        fields = ("id", "target", "user")


# class SimpleRDPSerializer(serializers.ModelSerializer):
#     environment = EnvironmentSerializer()
#
#     class Meta:
#         model = RemoteDesktop
#         fields = ("id", "name", "description", "ip", "environment")
#
#
# class SimpleRDPConnectionSerializer(serializers.ModelSerializer):
#     target = SimpleSecureShellSerializer()
#
#     class Meta:
#         model = RDPConnection
#         fields = ("id", "target", "user")


class ProfileSerializer(serializers.ModelSerializer):
    secure_shell_connections = SimpleSecureShellConnectionSerializer(many=True)
    database_connections = SimpleDatabaseConnectionSerializer(many=True)
    #rdp_connections = SimpleRDPConnectionSerializer(many=True)

    class Meta:
        model = Profile
        fields = "__all__"


class AppUserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = AppUser
        fields = ("id", "username", "first_name", "last_name", "enabled", "manual_ssh", "profile")


class AppUserEnabledStatusSerializer(serializers.ModelSerializer):
    not_allowed_environments = EnvironmentSerializer(many=True)

    class Meta:
        model = AppUser
        fields = ("id", "username", "enabled", "time_range_enabled", "time_range_enabled_since", "time_range_enabled_to", "not_allowed_environments")


class SessionRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = SessionRecord
        fields = "__all__"
